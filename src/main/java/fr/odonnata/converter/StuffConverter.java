package fr.odonnata.converter;

import fr.odonnata.model.Stuff;
import java.util.ArrayList;

public abstract class StuffConverter
{
    public Object getAsObject(ArrayList<Stuff> list, String itemId)
    {
        int entityId    = Integer.parseInt(itemId);
        Stuff ent      = new Stuff(entityId, "");
        return list.get(list.indexOf(ent));
    }

    public String getAsString(Object obj)
    {
        Stuff entity = (Stuff) obj;
        return entity.getId().toString();
    }
}