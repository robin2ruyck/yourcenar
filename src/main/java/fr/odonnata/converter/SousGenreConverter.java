package fr.odonnata.converter;

import fr.odonnata.bean.AppBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter("sousGenreConverter")
public class SousGenreConverter extends StuffConverter implements Converter
{
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s)
    {
        AppBean appBean = (AppBean) facesContext.getExternalContext().getSessionMap().get("appBean");
        return super.getAsObject(appBean.getSousGenres(), s);
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object obj)
    {
        return super.getAsString(obj);
    }
}
