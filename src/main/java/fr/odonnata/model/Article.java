package fr.odonnata.model;

import java.io.Serializable;
import java.util.Objects;

public class Article implements Serializable, Cloneable {
    private Integer id;
    private String libelle;
    private Stuff sousGenre;
    private Stuff editeur;
    private Stuff categorie;
    private Stuff genre;

    public Article() {
    }

    public Article(Integer id, String libelle) {
        this.id = id;
        this.libelle = libelle;
        this.sousGenre = null;
        this.editeur = null;
        this.categorie = null;
        this.genre = null;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Stuff getSousGenre() {
        return sousGenre;
    }

    public void setSousGenre(Stuff sousGenre) {
        this.sousGenre = sousGenre;
    }

    public Stuff getEditeur() {
        return editeur;
    }

    public void setEditeur(Stuff editeur) {
        this.editeur = editeur;
    }

    public Stuff getCategorie() {
        return categorie;
    }

    public void setCategorie(Stuff categorie) {
        this.categorie = categorie;
    }

    public Stuff getGenre() {
        return genre;
    }

    public void setGenre(Stuff genre) {
        this.genre = genre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Article article = (Article) o;
        return Objects.equals(id, article.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, libelle, sousGenre, editeur, categorie, genre);
    }

    @Override
    public String toString() {
        return libelle;
    }

    @Override
    public Article clone() throws CloneNotSupportedException {
        return (Article) super.clone();
    }
}
