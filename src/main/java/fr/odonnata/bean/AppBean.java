package fr.odonnata.bean;

import fr.odonnata.dao.DaoFactory;
import fr.odonnata.model.Article;
import fr.odonnata.model.Stuff;
import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import java.io.Serializable;
import java.util.ArrayList;

@ManagedBean
@ApplicationScoped
public class AppBean implements Serializable
{
    private ArrayList<Article> articles;
    private ArrayList<Stuff> categories;
    private ArrayList<Stuff> editeurs;
    private ArrayList<Stuff> etats;
    private ArrayList<Stuff> genres;
    private ArrayList<Stuff> sousGenres;

    public AppBean(){}

    @PostConstruct
    public void init()
    {
        articles = DaoFactory.getArticleDao().getAll("ARTICLE");
        categories = populateFromArticles(1);
        editeurs = populateFromArticles(2);
        genres = populateFromArticles(3);
        sousGenres = populateFromArticles(4);
    }

    private ArrayList<Stuff> populateFromArticles(int stuffVariableId)
    {
        ArrayList<Stuff> resultat =  new ArrayList<>();
        Stuff stuff = null;
        for (Article article : articles) {
            switch (stuffVariableId) {
                case 1: stuff = article.getCategorie();
                    break;
                case 2 : stuff = article.getEditeur();
                    break;
                case 3 : stuff = article.getGenre();
                    break;
                case 4 : stuff = article.getSousGenre();
                    break;
            }

            if (!resultat.contains(stuff))
                resultat.add(stuff);
        }
        return resultat;
    }

    public ArrayList<Article> getArticles() {
        return articles;
    }

    public void setArticles(ArrayList<Article> articles) {
        this.articles = articles;
    }

    public ArrayList<Stuff> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<Stuff> categories) {
        this.categories = categories;
    }

    public ArrayList<Stuff> getEditeurs() {
        return editeurs;
    }

    public void setEditeurs(ArrayList<Stuff> editeurs) {
        this.editeurs = editeurs;
    }

    public ArrayList<Stuff> getEtats() {
        return etats;
    }

    public void setEtats(ArrayList<Stuff> etats) {
        this.etats = etats;
    }

    public ArrayList<Stuff> getGenres() {
        return genres;
    }

    public void setGenres(ArrayList<Stuff> genres) {
        this.genres = genres;
    }

    public ArrayList<Stuff> getSousGenres() {
        return sousGenres;
    }

    public void setSousGenres(ArrayList<Stuff> sousGenres) {
        this.sousGenres = sousGenres;
    }
}