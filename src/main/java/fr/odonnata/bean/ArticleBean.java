package fr.odonnata.bean;

import fr.odonnata.dao.DaoFactory;
import fr.odonnata.model.Article;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;

@ManagedBean
@ViewScoped
public class ArticleBean implements Serializable
{
    private Article selectedArticle;
    private Article tmpArticle;

    public ArticleBean(){}

    public void onSelectArticle()
    {

        // TOTO : handle appbean.articles update
        if (selectedArticle != null)
        {
            selectedArticle = DaoFactory.getArticleDao().getByID(selectedArticle);
        }
    }

    public void onEditSaveButtonClicked()
    {
    }

    public void onDeleteButtonConfirmed()
    {
    }

    public void onCreateButtonClicked()
    {
    }

    public void onEditButtonClicked()
    {
    }

    public void onDeleteButtonClicked()
    {
    }

    public Article getSelectedArticle() {
        return selectedArticle;
    }

    public void setSelectedArticle(Article selectedArticle) {
        this.selectedArticle = selectedArticle;
    }

    public Article getTmpArticle() {
        return tmpArticle;
    }

    public void setTmpArticle(Article tmpArticle) {
        this.tmpArticle = tmpArticle;
    }


}
