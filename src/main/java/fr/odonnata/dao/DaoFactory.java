package fr.odonnata.dao;

import java.sql.Connection;

public class DaoFactory
{
    private static final Connection connexion = MsSQLConnect.getConnexion();


//    public static ExampleDao getExampleDao(){return new ExampleDao(connexion);}

    public static ArticleDao getArticleDao()
    {
        return new ArticleDao(connexion);
    }

}
