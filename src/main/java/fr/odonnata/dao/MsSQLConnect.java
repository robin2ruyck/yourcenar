package fr.odonnata.dao;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;

import java.sql.Connection;

public class MsSQLConnect
{
    // Declare the JDBC objects.
    private static Connection connexion;

    private MsSQLConnect()
    {
        try
        {
            SQLServerDataSource ds = new SQLServerDataSource();
            ds.setServerName("localhost");
            ds.setPortNumber(1433);
            ds.setDatabaseName("GESTIOLIB");
            ds.setIntegratedSecurity(false);
            ds.setUser("sa");
            ds.setPassword("Pa$$w0rd");
            connexion = ds.getConnection();
        }

        // Handle any errors that may have occurred.
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static synchronized Connection getConnexion()
    {
        if (connexion == null)
            new MsSQLConnect();
        return connexion;
    }

}

