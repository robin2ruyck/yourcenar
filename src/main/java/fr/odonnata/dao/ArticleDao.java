package fr.odonnata.dao;

import fr.odonnata.model.Article;
import fr.odonnata.model.Stuff;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class ArticleDao extends DAO<Article>
{
    public ArticleDao(Connection connexion)
    {
        super(connexion);
    }

    @Override
    public Article getByID(Article object) {
        ResultSet rs;
        Article article = null;
        try {
            PreparedStatement pstmt = connexion.prepareStatement(
                    "SELECT ID_ARTICLE, LIBELLE, ID_EDITEUR, EDITEUR, ID_SOUSGENRE,SOUSGENRE " +
                            "FROM articleView WHERE ID_ARTICLE = ?"
            );
            pstmt.setInt(1, object.getId());
            rs = pstmt.executeQuery();
            if(rs.next()) {
                Article newArticle = new Article(rs.getInt(1), rs.getString(2));
                newArticle.setEditeur(new Stuff(rs.getInt(3), rs.getString(4)));
                newArticle.setSousGenre(new Stuff(rs.getInt(5), rs.getString(6)));
                article = object;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return article;
    }

    @Override
    public ArrayList<Article> getAll(String dbTable) {
        ResultSet rs;
        ArrayList<Article> articles = new ArrayList<>();
        try {
            PreparedStatement pstmt = connexion.prepareStatement(
                    "SELECT ID_ARTICLE, LIBELLE, ID_SOUSGENRE, SOUSGENRE, ID_GENRE, GENRE, ID_CATEGORIE, " +
                            "CATEGORIE, ID_EDITEUR, EDITEUR FROM articleView"
            );
            rs = pstmt.executeQuery();
            while(rs.next()) {
                Article newArticle = new Article(rs.getInt(1), rs.getString(2));
                newArticle.setSousGenre(new Stuff(rs.getInt(3), rs.getString(4)));
                newArticle.setGenre(new Stuff(rs.getInt(5), rs.getString(6)));
                newArticle.setCategorie(new Stuff(rs.getInt(7), rs.getString(8)));
                newArticle.setEditeur(new Stuff(rs.getInt(9), rs.getString(10)));
                articles.add(newArticle);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return articles;
    }

    @Override
    public boolean insert(Article object) {
        boolean success = false;
        Article article = new Article(0, "");
        try {
            PreparedStatement pstmt = connexion.prepareStatement(
                    "INSERT INTO ARTICLE VALUES (?,?,?)",
                    Statement.RETURN_GENERATED_KEYS
            );
            pstmt.setString(1, object.getLibelle());
            pstmt.setInt(2, object.getSousGenre().getId());
            pstmt.setInt(3, object.getEditeur().getId());
            pstmt.executeUpdate();
            ResultSet rs = pstmt.getGeneratedKeys();
            success = rs.next();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return success;
    }

    @Override
    public boolean update(Article object) {
        try {
            PreparedStatement pstmt = connexion.prepareStatement(
                    "UPDATE ARTICLE SET LIBELLE = ?, ID_SOUSGENRE = ?, ID_EDITEUR = ? WHERE ID_ARTICLE = ?"
            );
            pstmt.setString(1, object.getLibelle());
            pstmt.setInt(2, object.getEditeur().getId());
            pstmt.setInt(3, object.getSousGenre().getId());
            pstmt.setInt(4, object.getId());
            return pstmt.executeUpdate() != 0;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean delete(Article object) {
        try {
            PreparedStatement pstmt = connexion.prepareStatement(
                    "DELETE FROM ARTICLE WHERE ID_ARTICLE = ?"
            );
            pstmt.setInt(1, object.getId());
            System.out.println(object.getId());
            return pstmt.executeUpdate() != 0;
        } catch (Exception e) {
            return false;
        }
    }
}
