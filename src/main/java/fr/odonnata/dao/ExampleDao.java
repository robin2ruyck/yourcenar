package fr.odonnata.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class ExampleDao extends DAO<Object>
{
    public ExampleDao(Connection connexion)
    {
        super(connexion);
    }

    @Override
    public Object getByID(Object object) {
        return null;
    }

    @Override
    public ArrayList<Object> getAll(String dbTable) {
        return null;
    }

    @Override
    public boolean insert(Object object) {
        return false;
    }

    @Override
    public boolean update(Object object) {
        return false;
    }

    @Override
    public boolean delete(Object object) {
        return false;
    }

}
