package fr.odonnata.dao;

import java.sql.Connection;
import java.util.ArrayList;

public abstract class DAO <T>
{
    protected Connection connexion;

    public DAO(Connection connexion)
    {
	this.connexion = connexion;
    }

    public Connection getConnexion()
    {
	return connexion;
    }

    public abstract T getByID(T object);
    public abstract ArrayList <T> getAll(String dbTable);
    public abstract boolean insert(T object);
    public abstract boolean update(T object);
    public abstract boolean delete( T object);
}

